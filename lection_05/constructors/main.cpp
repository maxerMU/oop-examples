#include <iostream>
#include "Array.h"

void CallsCopyConstructor(Array)
{
}

void DoNotCallsCopyConstructor(Array &)
{
}

int main()
{
    Array ar(5);
    CallsCopyConstructor(ar);
    DoNotCallsCopyConstructor(ar);

    Array ar1(std::move(ar));
}