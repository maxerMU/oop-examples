#include "Array.h"
#include <iostream>

Array::Array( int cnt )
  : count( cnt )
{
    std::cout << "Array::Array(int cnt)" << std::endl;
    arr = new double[count];
}

Array::Array( const Array& ar )
  : count( ar.count )
{
    std::cout << "Array::Array(const Array &ar)" << std::endl;
    arr = new double[count];

    for ( int i = 0; i < count; ++i )
        arr[i] = ar.arr[i];
}

Array::Array( Array&& ar ) noexcept
  : count( ar.count )
{
    std::cout << "Array::Array(Array &&ar)" << std::endl;
    arr = ar.arr;
    ar.arr = nullptr;
}

Array::~Array()
{
    delete[] arr;
}

bool
Array::equals( Array ar )
{
    if ( count != ar.count )
        return false;

    int i;
    for ( i = 0; i < count && arr[i] == ar.arr[i]; ++i )
        ;

    return i == count;
}

Array
Array::minus( const Array& ar )
{
    Array temp( ar );

    for ( int i = 0; i < temp.count; ++i )
        temp.arr[i] *= -1;

    return temp;
}