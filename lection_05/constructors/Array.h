#pragma once

class Array
{
public:
    Array() = default;
    Array(int cnt);
    Array(const Array& ar);
    Array(Array&& ar) noexcept;
    ~Array();

    bool equals(Array ar);

    static Array minus(const Array& ar);

private:
    double* arr = nullptr;
    int count = 0;
};