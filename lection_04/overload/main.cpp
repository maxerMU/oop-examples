#include <iostream>

void foo(int x) { std::cout << "int x: " << x << std::endl; }
void foo(double x) { std::cout << "double x: " << x << std::endl; }

void func1(int &) { std::cout << "func1(int&)" << std::endl; }
void func1(const int &) { std::cout << "func1(const int&)" << std::endl; }

void func2(const int &) { std::cout << "func2(const int&)" << std::endl; }
void func2(int &&) { std::cout << "func2(int&&)" << std::endl; }

int main() {
    foo(5);
    foo(5.0);

    int i = 0;
    const int ci = 0;
    int &lv = i;
    const int &clv = ci;
    int &&rv = i + 1;

    func1(i);     // int&
    func1(ci);    // const int&
    func1(lv);    // int&
    func1(clv);   // const int&
    func1(rv);    // int&
    func1(i + 1); // const int&

    func2(i);     // const int&
    func2(ci);    // const int&
    func2(lv);    // const int&
    func2(clv);   // const int&
    func2(rv);    // const int&
    func2(i + 1); // int&&
}