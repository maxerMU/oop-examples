#include <iostream>

void LValues() 
{
    int i = 0;

    int &lref1 = i;
    int &lref2(i);
    int &lref3{i};
    int &lref4 = {i};

    // int& lv2 = 2;        // Error!	(не lvalue)
    // int& lv3 = i + 1;    // Error!	(не lvalue)
    const int &lv4 = 2 + 1; // Ok!		(создается объект)

    lref1 += 1;
    std::cout << "lvalues: " << i << " " << lref1 << " " << lref2 << " " << lref3 << " " << lref4 << " " << lv4 << std::endl;
}

void RValues() {
    int i = 0;

    // int&& rv1 = i;        // Error!	(не rvalue)
    int &&rv2 = 2;           // Ok!
    int &&rv3 = i + 1;       // Ok!
    const int &&rv4 = i + 1; // Ok!
    ++rv2;                   // Ok!
    std::cout << "rvalues: " << rv2 << " " << rv3 << " " << rv4 << std::endl;

    // int &&rv5 = rv2; // Error!	(не rvalue)
    int &lv5 = rv2;  // Ok!
    lv5++;
    std::cout << "lv5 and rv2: " << lv5 << " " << rv2 << std::endl;

    int &&rv6 = (int)i;       // Ok!	( int(i) )
    int &&rv7 = std::move(i); // Ok!
    std::cout << "rv6 and rv7: " << rv6 << " " << rv7 << std::endl;
}

int main()
{
    LValues();
    RValues();
}